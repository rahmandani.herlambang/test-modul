<?php

namespace app\controllers;

use yii\data\Pagination;
use app\models\Billing;
use app\models\BillingSearch;
use Yii;

class BillingController extends \yii\web\Controller
{
  public function actionIndex()
  {
      $searchModel = new BillingSearch();
      $searchModel -> status = 0;
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

      return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
      ]);
  }

  public function actionPaid()
  {
      $searchModel = new BillingSearch();
      $searchModel -> status = 1;
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

      return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
      ]);
  }

}
