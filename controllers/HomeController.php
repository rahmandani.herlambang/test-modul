<?php

namespace app\controllers;

use Yii;
use yii\db\Query;
use app\models\Billing;
use yii\helpers\Url;
use app\models\SpptSearch;
use app\models\Sppt;
use app\components\BniHashing;

class HomeController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;

    public function actionIndex()
    {
        return $this->render('index', ['model' => new SpptSearch]);
    }

    public function actionCari()
    {
        $searchModel = new SpptSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $data = $dataProvider->getModels();
        return $this->render('view', [
            'model' => $searchModel,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

      }

    public function actionView($KD_PROPINSI, $KD_DATI2, $KD_KECAMATAN, $KD_KELURAHAN, $KD_BLOK, $NO_URUT, $KD_JNS_OP, $THN_PAJAK_SPPT , $nop)
    {
        $sppt = $this->findModel($KD_PROPINSI, $KD_DATI2, $KD_KECAMATAN, $KD_KELURAHAN, $KD_BLOK, $NO_URUT, $KD_JNS_OP, $THN_PAJAK_SPPT);
        $biller = new Billing;
        $biller->nop = $nop;
        $biller->nominal = $sppt['PBB_YG_HARUS_DIBAYAR_SPPT'];
        return $this->render('detail', [
            'model' => $sppt ,
            'biller' => $biller,
        ]);

    }

    // public function Hash()
    // {
    //   // "client_id" ,
    //   // "trx_amount" ,
    //   // "customer_name" ,
    //   // "customer_phone" ,
    //   // "customer_email",
    //   // "virtual_account" ,
    //   // "trx_id" ,
    //   // "datetime_expired" ,
    //   // "description" ,
    //   // "billing_type" ,
    //   // "type"
    //   $data = array(
    //     "client_id" => 00018
    //     ,"trx_amount" =>
    //     ,"customer_name" =>
    //     ,"customer_phone" =>
    //     ,"customer_email" =>
    //     ,"virtual_account" =>
    //     ,"trx_id" =>
    //     ,"datetime_expired" =>
    //     ,"description" =>
    //     ,"billing_type" =>
    //     ,"type" =>
    //   );
    // }

    public function Hit($data)
    {
      $url = 'http://172.20.2.84/bni-ecollection/_api/';

      $curl = curl_init();
      curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $data,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_HTTPHEADER => array("content-type: application/json"),
      ));

      $result = curl_exec($curl);

      return $result;
    }

    public function actionCreate()
    {

          $no = mt_rand(11111111,99999999);
          $format = "988".'00018'.$no;

          $datenow = date('Y-m-d H:i:s');

          $request = Yii::$app->request;

          $bill = new Billing();
          $bill-> load(Yii::$app->request->post());
          $bill->va =  str_pad($format, 16,0,STR_PAD_LEFT);
          $bill->status =  0;
          $bill->created_date = date('Y-m-d H:i:s');
          $bill->expired_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' +1 day'));


          $data = array(
            "client_id" => '00018' ,
            "trx_amount" => $bill->nominal ,
            "customer_name" => 'noname'
            ,"customer_phone" => ''
            ,"customer_email" => ''
            ,"virtual_account" => $bill->va
            ,"trx_id" => $bill->va
            ,"datetime_expired" => $bill->expired_date
            ,"description" => ''
            ,"billing_type" => 'c'
            ,"type" => 'createbilling'
          );

          // print_r($data);

          $dataJson = json_encode($data);
          $dataHash = BniHashing::hashData($data ,'00018' , '58467eabfc8def624b3660cf1d8a90c3' );
          $dataDecrypter = BniHashing::parseData($dataHash  ,'00018'  , '58467eabfc8def624b3660cf1d8a90c3');

          $dataHit = array(
            'client_id' => '00018',
            'prefix' => '988',
            'data' => $dataHash ,
          );

          $dataHitJson = json_encode($dataHit);
          $resultHit = $this->Hit($dataHitJson);

          // print_r($resultHit);
          // exit();


          $bill->save();

          return $this->redirect(Url::to(['/index.php/home/index']));
      }

      protected function findModel($KD_PROPINSI, $KD_DATI2, $KD_KECAMATAN, $KD_KELURAHAN, $KD_BLOK, $NO_URUT, $KD_JNS_OP, $THN_PAJAK_SPPT)
      {
          if (($model = Sppt::findOne(['KD_PROPINSI' => $KD_PROPINSI, 'KD_DATI2' => $KD_DATI2, 'KD_KECAMATAN' => $KD_KECAMATAN, 'KD_KELURAHAN' => $KD_KELURAHAN, 'KD_BLOK' => $KD_BLOK, 'NO_URUT' => $NO_URUT, 'KD_JNS_OP' => $KD_JNS_OP, 'THN_PAJAK_SPPT' => $THN_PAJAK_SPPT])) !== null) {
              return $model;
          }

          throw new NotFoundHttpException('The requested page does not exist.');
      }

}
