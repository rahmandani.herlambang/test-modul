<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "billing".
 *
 * @property int $trx_id
 * @property string $nop
 * @property string $va
 * @property int $status
 * @property int $nominal
 * @property string $created_date
 * @property string $expired_date
 * @property string $transaction_date
 */
class Billing extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'billing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nop', 'va', 'status'], 'required'],
            [['status', 'nominal'], 'integer'],
            [['created_date', 'expired_date', 'transaction_date'], 'safe'],
            [['nop'], 'string', 'max' => 20],
            [['va'], 'string', 'max' => 16],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'trx_id' => 'Trx ID',
            'nop' => 'Nop',
            'va' => 'Va',
            'status' => 'Status',
            'nominal' => 'Nominal',
            'created_date' => 'Created Date',
            'expired_date' => 'Expired Date',
            'transaction_date' => 'Transaction Date',
        ];
    }
}
