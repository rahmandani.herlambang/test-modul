<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Billing;

/**
 * Billing2Search represents the model behind the search form of `app\models\Billing`.
 */
class BillingSearch extends Billing
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['trx_id', 'status', 'nominal'], 'integer'],
            [['nop', 'va', 'created_date', 'expired_date', 'transaction_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Billing::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'trx_id' => $this->trx_id,
            'status' => $this->status,
            'nominal' => $this->nominal,
            'created_date' => $this->created_date,
            'expired_date' => $this->expired_date,
            'transaction_date' => $this->transaction_date,
        ]);

        $query->andFilterWhere(['like', 'nop', $this->nop])
            ->andFilterWhere(['like', 'va', $this->va]);

        return $dataProvider;
    }
}
