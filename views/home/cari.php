<form method="post">
    <div class="form-group">
        <label for="">Nop</label>
        <input name="nop" type="text" class="form-control" id="" aria-describedby="Nop"
               value="<?= $_GET['nop'] ?>">
    </div>
    <div class="form-group">
        <label for="">Tahun</label>
        <input name="tahun" type="text" class="form-control" id="" aria-describedby="Tahun"
               value="<?= $data['THN_PAJAK_SPPT'] ?>">
    </div>
    <div class="form-group">
        <label for="">Nama Wajib Pajak</label>
        <input name="npwp" type="text" class="form-control" id="" aria-describedby="Npwp"
               value="<?= $data['NPWP_SPPT'] ?>">
    </div>
    <div class="form-group">
        <label for="">Alamat</label>
        <input name="alamat" type="text" class="form-control" id="" aria-describedby="alamat"
               value="<?= $data['JLN_WP_SPPT'] ?>">
    </div>
    <div class="form-group">
        <label for="">Kota</label>
        <input name="kota" type="text" class="form-control" id="" aria-describedby="kota"
               value="<?= $data['KOTA_WP_SPPT'] ?>">
    </div>
    <div class="form-group">
        <label for="">Keluarahan</label>
        <input name="kelurahan" type="text" class="form-control" id="" aria-describedby="kelurahan"
               value="<?= $data['KELURAHAN_WP_SPPT'] ?>">
    </div>
    <div class="form-group">
        Tagihan: <br>
        <input hidden name="tagihan" class="form-control" id="" aria-describedby="tagihan"
               value="<?= $data['PBB_YG_HARUS_DIBAYAR_SPPT'] ?>">
        &nbsp; Rp <?= number_format($data['PBB_YG_HARUS_DIBAYAR_SPPT'],0,",","."); ?>
        <br>
        Tanggal Jatuh Tempo: <br>
        &nbsp; <?= $data['TGL_JATUH_TEMPO_SPPT'] ?>
        <br>
        Status: <br>
        &nbsp; <?= $data['STATUS_PEMBAYARAN_SPPT'] ? "<span style='color: red'>Belum Dibayar</span>" : "Belum Lunas"?>
    </div>

    <div class="form-group">
        <button>Pilih Metode Pembayaran</button>
        <br>
        <input type="radio" name="met_payment" checked> E-collection <br>
        <input type="radio" name="met_payment"> Yap!
    </div>
    <div style="text-align: center">
        <button class="btn btn-primary">Submit</button>
    </div>
</form>
