<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sppt */

$this->title = $model->KD_PROPINSI;
$this->params['breadcrumbs'][] = ['label' => 'Sppts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sppt-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'NM_WP_SPPT',
            'JLN_WP_SPPT',
            'KELURAHAN_WP_SPPT',
            'KOTA_WP_SPPT',
            'TGL_JATUH_TEMPO_SPPT',
        ],
    ]) ?>

		<?php $form = ActiveForm::begin(['action'=>'create']); ?>

				<?= $form->field($biller, 'nop') ?>
				<?= $form->field($biller, 'nominal') ?>

				<div class="form-group">
						<?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
				</div>
		<?php ActiveForm::end(); ?>

</div>
