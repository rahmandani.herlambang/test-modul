<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $model app\models\SpptSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sppt-search">

    <?php $form = ActiveForm::begin([
        'action' => ['cari'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>
    <?php echo $form->field($model, 'nop') ?>

    <?php echo $form->field($model, 'THN_PAJAK_SPPT') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
