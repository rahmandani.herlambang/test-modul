<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SpptSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sppts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sppt-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'THN_PAJAK_SPPT',
            'NM_WP_SPPT',

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete} {myButton}',  // the default buttons + your custom button
            'buttons' => [
                'myButton' => function($url, $model, $key)use($searchModel) {     // render your custom button
                    return Html::a('detail', ['view','KD_PROPINSI'=> $model->KD_PROPINSI , 'KD_DATI2'=> $model->KD_DATI2 , 'KD_KECAMATAN'=> $model->KD_KECAMATAN
                  , 'KD_KELURAHAN'=> $model->KD_KELURAHAN , 'KD_BLOK'=> $model->KD_BLOK , 'NO_URUT'=> $model->NO_URUT , 'KD_JNS_OP'=> $model->KD_JNS_OP , 'THN_PAJAK_SPPT'=> $model->THN_PAJAK_SPPT
                  , 'nop'=>$searchModel->nop ], ['class' => 'btn btn-success btn-xs', 'data-pjax' => 0]);
                }
            ]]
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
