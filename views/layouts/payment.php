<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\StringHelper;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <!--    --><?php //$this->head() ?>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<?php $this->beginBody() ?>


<div class="container">
    <h2>Home</h2>
    <div class="bd-example bd-example-tabs">
        <div class="row">
            <div class="col-3">
                <div class="nav flex-column nav-pills" aria-orientation="vertical">
                    <a class="nav-link <?= Yii::$app->request->url == '/index.php/home/index' ? 'active' : '' ?>" href="<?= Url::toRoute('/index.php/home/index') ?>" role="tab"
                       aria-controls="v-pills-home" aria-selected="false">Home</a>
                    <a class="nav-link <?= Yii::$app->request->url == '/index.php/billing/index' ? 'active' : '' ?>"
                       href="<?= Url::toRoute('/index.php/billing/index') ?>" role="tab"
                       aria-controls="v-pills-profile" aria-selected="false">Billing</a>
                    <a class="nav-link <?= Yii::$app->request->url == '/index.php/billing/paid' ? 'active' : '' ?>"
                       href="<?= Url::toRoute('/index.php/billing/paid') ?>">Paid</a>
                </div>
            </div>
            <div class="col-9">
                <div class="tab-content" id="v-pills-tabContent">

                    <div class="tab-pane fade active show" id="content" role="tabpanel"
                         aria-labelledby="v-pills-messages-tab">
                        <?= $content ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
